export const ENV_PRODUCTION = '<生产环境云ID>'
    , ENV_DEVELOPMENT = '<开发环境云ID>'

// 初始化云环境
// wx.cloud.init({
//     env: ENV_DEVELOPMENT
// })

import * as fun from './callFunction'
import * as db from './database'

export const MODEL = fun.MODEL
    , FUNC = fun.FUNC
    , callFunction = fun.callFunction
    , getDocument = db.getDocument